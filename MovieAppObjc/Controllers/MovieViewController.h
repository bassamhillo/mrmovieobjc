//
//  ViewController.h
//  MovieAppObjc
//
//  Created by Julia on 8/17/20.
//  Copyright © 2020 Bassam. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DataManager.h"
#import "MovieTableViewCell.h"
#import "MovieTableView.h"
#import "DataManager.h"

@interface MovieViewController: UIViewController<UISearchBarDelegate, UISearchDisplayDelegate, DetailsViewDisplay, ImagesLoadedDelegate> {
    NSMutableArray *movies;
    NSMutableDictionary *images;

    DataManager *dataManager;    
}
- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText;
@end

