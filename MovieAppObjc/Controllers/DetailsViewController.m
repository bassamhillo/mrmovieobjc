//
//  DetailsViewController.m
//  MovieAppObjc
//
//  Created by Julia on 8/18/20.
//  Copyright © 2020 Bassam. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DetailsViewController.h"

@interface DetailsViewController()

@end

@implementation DetailsViewController

- (void)setMovie:(MovieModel *)movieModel withImage:(UIImage *)movieImage {
    movie = movieModel;
    coverImage = movieImage;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setTitle:[movie name]];
    // self.headerImage.image = coverImage;
    
    [self tableViewSetUp];
}

-(void) tableViewSetUp {
    UINib *nib = [UINib nibWithNibName:@"DetailsTableViewCell" bundle:nil];
    [self.detailsTableView registerNib:nib forCellReuseIdentifier:@"DetailsCell"];
    
    [self addDetailsTableHeader];
}

-(void) addDetailsTableHeader {
    DetailsTableViewHeader *tableHeader = [[[NSBundle mainBundle] loadNibNamed:@"DetailsTableViewHeader" owner:0 options:nil] objectAtIndex:0];
    
    tableHeader.detailsCoverImage.image = coverImage;
    header = tableHeader;
    
    [self.detailsTableView setTableHeaderView:nil];
    [self.detailsTableView addSubview:header];
    
    [self.detailsTableView setContentInset:UIEdgeInsetsMake(tableHeader.bounds.size.height, 0, 0, 0)];
    [self.detailsTableView setContentOffset:CGPointMake(0, -tableHeader.bounds.size.height)];
    [self.detailsTableView setMovie:movie withHeaderFrame:header];

}

@end
