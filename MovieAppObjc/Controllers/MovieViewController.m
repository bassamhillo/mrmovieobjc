//
//  ViewController.m
//  MovieAppObjc
//
//  Created by Julia on 8/17/20.
//  Copyright © 2020 Bassam. All rights reserved.
//

#import "MovieViewController.h"

@interface MovieViewController ()
@property (weak, nonatomic) IBOutlet MovieTableView *movieTableView;
@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;


@end

@implementation MovieViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.

    [self fetchMovies];
    [self tableViewSetUp];
}

- (void) fetchMovies {
    dataManager = [[DataManager alloc] init];
    dataManager.imagesDelegate = self;

    movies = [NSMutableArray array];
    [dataManager getMovies];

}

- (void) tableViewSetUp {
    UINib *nib = [UINib nibWithNibName:@"MovieTableViewCell" bundle:nil];
    [self.movieTableView registerNib:nib forCellReuseIdentifier:@"movieCell"];

    self.movieTableView.displayDetailsDelegate = self;
}

- (void) imagesLoaded: (NSMutableArray *) moviesArray {
    movies = moviesArray;
    [self.movieTableView setMovies:movies];
}



- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
    movies = [dataManager getMoviesThatContains:searchText];
    [self.movieTableView setMovies:movies];
}

- (void)displayDetailsView:(MovieModel *)movie withImage:(UIImage *)image {
    DetailsViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"DetailsViewController"];

    [vc setMovie:movie withImage:image];
    
    [[self navigationController] pushViewController:vc animated:false];
}



@end
