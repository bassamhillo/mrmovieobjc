//
//  DetailsViewController.h
//  MovieAppObjc
//
//  Created by Julia on 8/18/20.
//  Copyright © 2020 Bassam. All rights reserved.
//

#ifndef DetailsViewController_h
#define DetailsViewController_h


#endif /* DetailsViewController_h */

#import <UIKit/UIKit.h>
#import "MovieModel.h"
#import "DetailsTableView.h"
#import "DetailsTableViewHeader.h"

@interface DetailsViewController:UIViewController {
    MovieModel *movie;
    UIImage *coverImage;
    UIView *header;
}

@property (weak, nonatomic) IBOutlet UIImageView *headerImage;
@property (weak, nonatomic) IBOutlet DetailsTableView *detailsTableView;


- (void) setMovie:(MovieModel*) movieModel withImage:(UIImage*) movieImage;
@end
