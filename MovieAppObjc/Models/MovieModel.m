//
//  MovieModel.m
//  MovieAppObjc
//
//  Created by Julia on 8/17/20.
//  Copyright © 2020 Bassam. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MovieModel.h"

@implementation MovieModel

- (void) setMovie:(NSString*) movieName movieID:(NSString*) movieId type:(NSString*) movieType status:(NSString*) movieStatus url:(NSString*) movieURL {
    name = movieName;
    movieID = movieId;
    type = movieType;
    status = movieStatus;
    url = movieURL;
}

- (NSString *)name {
    return name;
}

- (NSString *)movieID {
    return movieID;
}

- (NSString *)type {
    return type;
}

- (NSString *)status {
    return status;
}

- (NSString *)url {
    return url;
}
- (NSString *) score {
    return score;
}

- (NSMutableArray *)genres {
    return genres;
}

- (NSDictionary *)schedule {
    return schedule;
}

- (void) setScore:(NSString *)movieScore {
    score = movieScore;
}

- (void) setGenres:(NSMutableArray *)movieGenres {
    genres = movieGenres;
}
- (void) setSchedule:(NSDictionary *)moviesShedule {
    schedule = moviesShedule;
}


@end
