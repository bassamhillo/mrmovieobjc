//
//  Movie.m
//  MovieAppObjc
//
//  Created by Julia on 8/20/20.
//  Copyright © 2020 Bassam. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Movie.h"

@interface Movie()

@end

@implementation Movie

-(id)init:(MovieModel*) movie andImage:(UIImage*) movieImage {
    self = [super init];
    if (self) {
        self->movieModel = movie;
        self->image = movieImage;

    }
    return self;
}

- (void) setImage:(UIImage *) image {
    self->image = image;
}


- (UIImage *) image {
    return image;
}

- (MovieModel *)movieModel {
    return movieModel;
}
@end
