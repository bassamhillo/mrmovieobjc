//
//  Movie.h
//  MovieAppObjc
//
//  Created by Julia on 8/20/20.
//  Copyright © 2020 Bassam. All rights reserved.
//

#ifndef Movie_h
#define Movie_h


#endif /* Movie_h */

#import <UIKit/UIKit.h>
#import "MovieModel.h"

@interface Movie : NSObject {
    MovieModel *movieModel;
    UIImage *image;
}

- (MovieModel *) movieModel;
- (UIImage *) image;
- (void) setImage:(UIImage *) image;
-(id)init:(MovieModel*) movie andImage:(UIImage*) movieImage;

@end

