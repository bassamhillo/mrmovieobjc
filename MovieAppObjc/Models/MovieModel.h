//
//  MovieModel.h
//  MovieAppObjc
//
//  Created by Julia on 8/17/20.
//  Copyright © 2020 Bassam. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MovieModel: NSObject {
    NSString *name;
    NSString *movieID;
    NSString *type;
    NSString *status;
    NSString *url;
    NSString *score;
    NSMutableArray *genres;
    NSDictionary *schedule;
}

- (void) setMovie:(NSString*) movieName movieID:(NSString*) movieId type:(NSString*) movieType status:(NSString*) movieStatus url:(NSString*) movieURL;
- (NSString *) name;
- (NSString *) movieID;
- (NSString *) type;
- (NSString *) status;
- (NSString *) url;
- (NSString *) score;
- (NSMutableArray *) genres;
- (NSDictionary *) schedule;

- (void) setScore:(NSString *)movieScore;
- (void) setGenres:(NSMutableArray *)movieGenres;
- (void) setSchedule:(NSDictionary *)moviesShedule;
@end
