//
//  DetailsViewUtil.h
//  MovieAppObjc
//
//  Created by Julia on 8/18/20.
//  Copyright © 2020 Bassam. All rights reserved.
//

#ifndef DetailsViewUtil_h
#define DetailsViewUtil_h


#endif /* DetailsViewUtil_h */
#import <Foundation/Foundation.h>

@interface DetailsViewUtil : NSObject {
    NSMutableArray *sections;
}
- (NSMutableArray*) sections;
@end
