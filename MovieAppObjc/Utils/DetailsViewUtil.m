//
//  DetailsViewUtil.m
//  MovieAppObjc
//
//  Created by Julia on 8/18/20.
//  Copyright © 2020 Bassam. All rights reserved.
//

#import "DetailsViewUtil.h"

@interface DetailsViewUtil()

@end

@implementation DetailsViewUtil

- (instancetype)init
{
    self = [super init];
    if (self) {
        sections = [NSMutableArray array];
        id sharedKeySet = [NSDictionary sharedKeySetForKeys:@[@"title", @"cellCount"]]; // returns
        NSMutableDictionary *name = [NSMutableDictionary dictionaryWithSharedKeySet:sharedKeySet];
        name[@"title"] =@"Name";
        name[@"cellCount"] = @1;
        [sections addObject:name];
        
        NSMutableDictionary *type = [NSMutableDictionary dictionaryWithSharedKeySet:sharedKeySet];
        type[@"title"] =@"TYPE";
        type[@"cellCount"] = @1;
        [sections addObject:type];

        NSMutableDictionary *score = [NSMutableDictionary dictionaryWithSharedKeySet:sharedKeySet];
        score[@"title"] =@"SCORE";
        score[@"cellCount"] = @1;
        [sections addObject:score];


        NSMutableDictionary *genres = [NSMutableDictionary dictionaryWithSharedKeySet:sharedKeySet];
        genres[@"title"] =@"GENRES";
        genres[@"cellCount"] = @1;
        [sections addObject:genres];

        NSMutableDictionary *status = [NSMutableDictionary dictionaryWithSharedKeySet:sharedKeySet];
        status[@"title"] =@"STATUS";
        status[@"cellCount"] = @1;
        [sections addObject:status];

        NSMutableDictionary *sced = [NSMutableDictionary dictionaryWithSharedKeySet:sharedKeySet];
        sced[@"title"] =@"SCHEDULE";
        sced[@"cellCount"] = @2;
        [sections addObject:sced];

    }
    return self;
}

- (NSMutableArray *)sections {
    return sections;
}
@end
