//
//  MovieTableView.h
//  MovieAppObjc
//
//  Created by Julia on 8/17/20.
//  Copyright © 2020 Bassam. All rights reserved.
//
#import <UIKit/UIKit.h>
#import "MovieTableViewCell.h"
#import "MovieModel.h"
#import "DetailsViewController.h"
#import "Movie.h"
#import <Foundation/Foundation.h>

@protocol DetailsViewDisplay <NSObject>
- (void) displayDetailsView: (MovieModel *) movie withImage:(UIImage*)image;
@end


@interface MovieTableView: UITableView<UITableViewDataSource, UITableViewDelegate> {
    NSMutableArray *movies;
}
@property (nonatomic, weak) id <DetailsViewDisplay> displayDetailsDelegate;
- (void) setMovies:(NSMutableArray*) moviesToAdd;
@end
