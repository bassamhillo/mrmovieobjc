//
//  DetailsTableViewHeader.h
//  MovieAppObjc
//
//  Created by Julia on 8/19/20.
//  Copyright © 2020 Bassam. All rights reserved.
//

#ifndef DetailsTableViewHeader_h
#define DetailsTableViewHeader_h


#endif /* DetailsTableViewHeader_h */

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface DetailsTableViewHeader : UITableViewCell {
    
}
@property (weak, nonatomic) IBOutlet UIImageView *detailsCoverImage;

@end
