//
//  DetailsTableViewHeader.m
//  MovieAppObjc
//
//  Created by Julia on 8/19/20.
//  Copyright © 2020 Bassam. All rights reserved.
//

#import "DetailsTableViewHeader.h"

@interface DetailsTableViewHeader()

@end

@implementation DetailsTableViewHeader
- (id) initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}
@end
