//
//  DetailsTableView.m
//  MovieAppObjc
//
//  Created by Julia on 8/18/20.
//  Copyright © 2020 Bassam. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DetailsTableView.h"

@interface DetailsTableView()

@end

@implementation DetailsTableView

- (void) setMovie:(MovieModel*) movieModel withHeaderFrame:(UIView*)header {
    movie = movieModel;
    headerView = header;
    
    detailsViewUtil = [[DetailsViewUtil alloc]init];
    sections = detailsViewUtil.sections;
    
    [self updateHeaderView];
    
    self.delegate = self;
    self.dataSource = self;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    
    UILabel *myLabel = [[UILabel alloc] init];
    myLabel.frame = CGRectMake(20, 5, 320, 20);
    
    myLabel.font = [UIFont systemFontOfSize:15];
    myLabel.textColor = UIColor.grayColor;
    myLabel.text = [self tableView:tableView titleForHeaderInSection:section];

    UIView *headerView = [[UIView alloc] init];
    [headerView addSubview:myLabel];

    return headerView;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return sections.count;
}
- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    return [sections objectAtIndex:section][@"title"];
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [[sections objectAtIndex:section][@"cellCount"] intValue];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    DetailsTableViewCell *cell = (DetailsTableViewCell*) [tableView dequeueReusableCellWithIdentifier:@"DetailsCell"];
    
    [cell setIndentationLevel:2];
    NSInteger sectionNumber = indexPath.section;
    switch (sectionNumber) {
        case 0:
            cell.valueLabel.text = movie.name;
            break;
        case 1:
              cell.valueLabel.text = movie.type;
              break;
        case 2:
            cell.valueLabel.text =  movie.score;
              break;
        case 3:
              cell.valueLabel.text = [movie.genres componentsJoinedByString:@", "];
              break;
        case 4:
              cell.valueLabel.text = movie.status;
              break;
        case 5:
            if (indexPath.item == 0){
                cell.valueLabel.text = [movie.schedule objectForKey:@"time"];
            } else {
                NSMutableArray *dayes =[movie.schedule objectForKey:@"days"];
                cell.valueLabel.text = [dayes componentsJoinedByString:@", "];
            }
              break;
            
        default:
            break;
    }
    [cell layer].borderWidth =  1.0f;
    [cell layer].borderColor = [UIColor lightGrayColor].CGColor;

    return cell;
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    [self updateHeaderView];
}

- (void) updateHeaderView {
    CGRect headerRect = CGRectMake(0, -headerView.bounds.size.height, self.bounds.size.width, headerView.bounds.size.height);
    if (self.contentOffset.y < -headerView.bounds.size.height) {
        headerRect.origin.y = self.contentOffset.y;
        headerRect.size.height = -self.contentOffset.y;
    }
    
    headerView.frame = headerRect;
}



@end
