//
//  DetailsTableViewCell.h
//  MovieAppObjc
//
//  Created by Julia on 8/18/20.
//  Copyright © 2020 Bassam. All rights reserved.
//

#ifndef DetailsTableViewCell_h
#define DetailsTableViewCell_h



#endif /* DetailsTableViewCell_h */

#import <UIKit/UIKit.h>

@interface DetailsTableViewCell: UITableViewCell {
    
}
@property (weak, nonatomic) IBOutlet UIImageView *logoImage;
@property (weak, nonatomic) IBOutlet UILabel *valueLabel;

@end
