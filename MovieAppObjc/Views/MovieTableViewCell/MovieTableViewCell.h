//
//  MovieTableViewCell.h
//  MovieAppObjc
//
//  Created by Julia on 8/17/20.
//  Copyright © 2020 Bassam. All rights reserved.
//

#ifndef MovieTableViewCell_h
#define MovieTableViewCell_h


#endif /* MovieTableViewCell_h */

#import <UIKit/UIKit.h>
#import "HCSStarRatingView.h"

@interface MovieTableViewCell: UITableViewCell {
    
}
@property (weak, nonatomic) IBOutlet UILabel *movieName;
@property (weak, nonatomic) IBOutlet UILabel *movieType;
@property (weak, nonatomic) IBOutlet UIImageView *movieImage;
@property (weak, nonatomic) IBOutlet HCSStarRatingView *movieRaiting;

@end
