//
//  DetailsTableView.h
//  MovieAppObjc
//
//  Created by Julia on 8/18/20.
//  Copyright © 2020 Bassam. All rights reserved.
//

#ifndef DetailsTableView_h
#define DetailsTableView_h


#endif /* DetailsTableView_h */

#import <UIKit/UIKit.h>
#import "MovieModel.h"
#import "DetailsTableViewCell.h"
#import "DetailsViewUtil.h"
#import "DetailsTableViewHeader.h"

@interface DetailsTableView: UITableView<UITableViewDataSource, UITableViewDelegate> {
    MovieModel *movie;
    DetailsViewUtil *detailsViewUtil;
    NSMutableArray *sections;
    UIView *headerView;
}



- (void) setMovie:(MovieModel*) movieModel withHeaderFrame:(UIView*)header;
@end
