//
//  MovieTableView.m
//  MovieAppObjc
//
//  Created by Julia on 8/17/20.
//  Copyright © 2020 Bassam. All rights reserved.
//

#import "MovieTableView.h"



@interface MovieTableView() 
@property (strong, nonatomic) IBOutlet MovieTableViewCell *customCell;

@end

@implementation MovieTableView

- (void) setMovies:(NSMutableArray*) moviesToAdd{
    self.dataSource = self;
    self.delegate = self;

    movies = moviesToAdd;
    [self reloadData];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return movies.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    MovieTableViewCell *cell =(MovieTableViewCell *) [tableView dequeueReusableCellWithIdentifier:@"movieCell"];
    
    Movie *movieObject = movies[indexPath.row];
    MovieModel *movie = movieObject.movieModel;
    cell.movieName.text = movie.name;
    cell.movieType.text = movie.type;
    cell.movieImage.image = movieObject.image;
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    int score = ([movie.score intValue]/25.0) * 5.0;
    cell.movieRaiting.value = score;
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
   return 100;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
        
    Movie *movieObject = movies[indexPath.row];
    MovieModel *movie = movieObject.movieModel;
    UIImage *image = movieObject.image;
    
    if (_displayDetailsDelegate){
        [_displayDetailsDelegate displayDetailsView:movie withImage:image];
    }
}
@end


