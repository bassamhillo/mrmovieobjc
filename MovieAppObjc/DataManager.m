//
//  DataManager.m
//  MovieAppObjc
//
//  Created by Julia on 8/17/20.
//  Copyright © 2020 Bassam. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "DataManager.h"
#import "MovieModel.h"

@implementation DataManager

- (void) getMovies {
    moviesModelArray = [NSMutableArray array];
    if(moviesArray){
        [moviesArray removeAllObjects];
    }
    NSDictionary *movies = [self JSONFromFile];
    for (NSDictionary *movie in movies) {
        MovieModel *movieModel = [[MovieModel alloc]init];
        NSDictionary *show = [movie objectForKey:@"show"];
        
        NSDictionary *image = [show objectForKey:@"image"];
        NSString *url;
        if(image){
            url = [image objectForKey:@"medium"];
        } else {
            url = @"";
        }
        
        [movieModel setMovie:[show objectForKey:@"name"] movieID:[show objectForKey:@"id"] type:[show objectForKey:@"type"] status:[show objectForKey:@"status"] url:url];
        NSString *score = [[movie objectForKey:@"score"] stringValue];
        [movieModel setScore:score];
        
        NSMutableArray *genres = [show objectForKey:@"genres"];
        [movieModel setGenres:genres];
        
        NSDictionary *schedule = [show objectForKey:@"schedule"];
        [movieModel setSchedule:schedule];
        
        [moviesModelArray addObject:movieModel];
    }
    [self fetchImages];
}

- (void) fetchImages {
    if (!moviesArray || moviesArray.count <= 0) {
        moviesArray = [NSMutableArray array];
        __block NSInteger imagesCount = 0;
        for (MovieModel *movie in moviesModelArray) {
            if (movie.url != nil && ![movie.url  isEqual: @""]) {
                imagesCount += 1;
            }
        }

        __block NSInteger doneImagesCount = 0;
        for (MovieModel *movie in moviesModelArray) {
            Movie *newMovie = [[Movie alloc] init:movie andImage:nil];
            [self->moviesArray addObject:newMovie];
            dispatch_async(dispatch_get_global_queue(0,0), ^{
                NSData * data = [[NSData alloc] initWithContentsOfURL: [NSURL URLWithString:movie.url]];
                if ( data == nil )
                    return;
                dispatch_async(dispatch_get_main_queue(), ^{
                    // WARNING: is the cell still using the same data by this point??
                    [newMovie setImage:[UIImage imageWithData: data]];
                    doneImagesCount += 1;
                    if (doneImagesCount >= imagesCount){
                        [self->_imagesDelegate imagesLoaded:self->moviesArray];
                    }
                });
            });
        }
    }
}

- (NSMutableArray *) getMoviesThatContains:(NSString *) movieNameSearch {
    if ([movieNameSearch isEqualToString:@""]) {
        return moviesArray;
    }
    
    if (!searchMovies) {
        searchMovies = [NSMutableArray array];
    } else {
        [searchMovies removeAllObjects];
    }
    movieNameSearch = [movieNameSearch lowercaseString];
    for (Movie *movie in moviesArray) {
        if ([[movie.movieModel.name lowercaseString] containsString:movieNameSearch]) {
            [searchMovies addObject:movie];
        }
    }
    return searchMovies;
}

- (NSDictionary *)JSONFromFile
{
    NSString *path = [[NSBundle mainBundle] pathForResource:@"Movie" ofType:@"json"];
    NSData *data = [NSData dataWithContentsOfFile:path];
    return [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
}

@end
