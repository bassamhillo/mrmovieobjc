//
//  DataManager.h
//  MovieAppObjc
//
//  Created by Julia on 8/17/20.
//  Copyright © 2020 Bassam. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Movie.h"

@protocol ImagesLoadedDelegate <NSObject>

- (void) imagesLoaded: (NSMutableArray *) moviesArray;

@end

@interface DataManager : NSObject {
    NSMutableArray *moviesModelArray;
    NSMutableArray *moviesArray;
    NSMutableArray *searchMovies;
    
}
@property (nonatomic, weak) id <ImagesLoadedDelegate> imagesDelegate;
- (void) getMovies;
- (NSMutableArray *) getMoviesThatContains:(NSString *) movieNameSearch;
- (NSDictionary *) JSONFromFile;
@end
